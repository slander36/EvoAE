@echo OFF

echo Running WINE dataset
python ..\..\src\EvoAE.py -n -c wine.config ..\..\data\wine\wine_training.csv ..\..\data\wine\wine_testing.csv

echo Finished running tests