@echo OFF

echo Running feature reduction

python ..\..\src\FeatureReducer.py -c terms.config "..\..\data\terms\terms.csv" --verbose

echo Finished feature reduction