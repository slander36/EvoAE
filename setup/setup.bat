@echo OFF

:All

IF NOT DEFINED VS120COMNTOOLS GOTO NO12

IF NOT DEFINED VS110COMNTOOLS SET VS110COMNTOOLS=%VS120COMNTOOLS%

IF NOT DEFINED VS100COMNTOOLS SET VS100COMNTOOLS=%VS120COMNTOOLS%

IF NOT DEFINED VS90COMNTOOLS SET VS90COMNTOOLS=%VS120COMNTOOLS%

GOTO Success

:NO12

IF NOT DEFINED VS110COMNTOOLS GOTO NO11

IF NOT DEFINED VS100COMNTOOLS SET VS100COMNTOOLS=%VS110COMNTOOLS%

IF NOT DEFINED VS90COMNTOOLS SET VS90COMNTOOLS=%VS110COMNTOOLS%

GOTO Success

:NO11

IF NOT DEFINED VS100COMNTOOLS GOTO NO10

IF NOT DEFINED VS90COMNTOOLS SET VS90COMNTOOLS=%VS100COMNTOOLS%

GOTO Success

:NO10

IF NOT DEFINED VS90COMNTOOLS GOTO Failure

GOTO Success

:Failure

echo Failed to set environment variables. Please install Visual Studio

GOTO End

:Success

echo Successfully set environment variables! Installing python modules

pip install ..\wheelhouse\numpy-1.9.2rc1+mkl-cp34-none-win_amd64.whl
pip install ..\wheelhouse\scipy-0.15.1-cp34-none-win_amd64.whl
pip install ..\wheelhouse\scikit_learn-0.15.2-cp34-none-win_amd64.whl
pip install ..\wheelhouse\matplotlib-1.4.3-cp34-none-win_amd64.whl

GOTO End

:End

echo Setup Complete. If no errors occured you're good to go
