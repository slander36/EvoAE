@echo OFF

pip install ..\wheelhouse\numpy-1.9.2rc1+mkl-cp34-none-win_amd64.whl
pip install ..\wheelhouse\scipy-0.15.1-cp34-none-win_amd64.whl
pip install ..\wheelhouse\scikit_learn-0.15.2-cp34-none-win_amd64.whl
pip install ..\wheelhouse\matplotlib-1.4.3-cp34-none-win_amd64.whl