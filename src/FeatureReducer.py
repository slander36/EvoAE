'''

Feature Reducer - Uses EvoAE to reduce feature size

@author Sean Lander
@copyright 2014
@title FeatureReducer

Training data should be a csv file with the first
column containing the label (number or string), with the
following columns containing the feature data.

Adding the -normalize flag will cause all data to be normalized
using its z-score.

Training data will be used to build a deep learning network
using meta-parameters found in the configuration file (config.json)

'''

import numpy as np

import os
import argparse
from datetime import datetime

import time

import EvoAE
from EvoAutoEncoder import EvoAutoEncoder
from EvoConfig import EvoConfig
from utils import normalize

def main():
	(alldata,config) = parseInput()
	num_samples = alldata['features'].shape[0]
	[training,_,validation] = np.split(alldata['features'],[int(num_samples*0.8),int(num_samples*0.8)])
	training = training.T
	validation = validation.T
	(best_pop,rankings,_) = EvoAE.main(training,validation,config)

	rankings = sorted(rankings.items(), key=lambda x : x[1])
	best_ae = best_pop[rankings[0][0]]
	f = open('%sAE.weights.json' % (config.output),'w')
	EvoAutoEncoder.encode(best_ae,f)
	f.close()

	(_,output,_,_) = best_ae.feedforward(alldata['features'].T)
	output = output.T.tolist()
	f = open('%sReduced.csv' % (config.output),'w')
	for i in range(len(output)):
		f.write("%s,%s\n" % (alldata['ids'][i],','.join(str(x) for x in output[i])))
	f.close()

def parseInput():
	parser = argparse.ArgumentParser(description="Runner for Evolutionary AutoEncoder training and testing")

	data_group = parser.add_argument_group('Data')
	data_group.add_argument("training",type=argparse.FileType(),help="data for training")
	
	config_group = parser.add_argument_group('Configuration')
	config_group.add_argument("-a","--alpha",help="learning rate",type=float)
	config_group.add_argument("-m","--momentum",help="momentum term for congugate gradient descence (0 to turn off)",type=float)
	config_group.add_argument("-l","--lamb",help="lambda term for weight decay (0 to turn off)",type=float)
	config_group.add_argument("-s","--hsize",help="size of the hidden layer",type=int)
	config_group.add_argument("-d","--hstddev",help="stddev of hidden layer size on initialization",type=float)
	config_group.add_argument("-+","--hplusminus",help="+/- of hidden layer size on initialization",type=int)
	config_group.add_argument("-u","--mutation",help="mutation rate [0,1] scale",type=float)
	config_group.add_argument("-p","--populationsize",help="number of organisms in the population")
	config_group.add_argument("-g","--generations",help="number of generations")
	config_group.add_argument("-e","--epochs",help="numer of search iterations")
	config_group.add_argument("--batchsize",help="the size of each batch used in training (optional)",type=int)
	config_group.add_argument("--autobatch",help="splits data into populationsize many batches (optional)",action='store_true')
	config_group.add_argument("--minibatch",help="splits data into populationsize many batches, with each member training against a single batch (optional)",action='store_true')
	config_group.add_argument("--finaltrain",help="specify if you want a final round of training after convergence and which type (optional)",choices=['All','Batch','None'],default='None')
	config_group.add_argument("-n","--normalize",help="flag for if data needs to be normalized before training",action='store_true')
	config_group.add_argument("-o","--output",help="output directory name")
	config_group.add_argument("-t","--name",help="name of experiment")
	config_group.add_argument("-v","--verbose",help="verbose output",action='store_true')

	parser.add_argument("-c","--config",type=argparse.FileType('r'),help="configuration file")

	args = parser.parse_args()
	print("ArgumentParser complete")

	training = np.loadtxt(args.training,delimiter=',')
	print("Loadtxt complete")
	
	np.random.shuffle(training)
	print("Shuffle complete")

	training = {'ids':training[:,0],'features':training[:,1:]}
	print("Partition into ids and features complete")
	
	insize = training['features'].shape[1]

	if args.config:
		config = EvoConfig(config=args.config)
	else:
		config = EvoConfig()

	# Training Params
	if args.alpha:
		config.alpha = args.alpha
	if args.momentum:
		config.momentum = args.momentum
	if args.lamb:
		config.lamb = args.lamb
	
	# Structure Params
	if args.hsize:
		config.hsize = args.hsize
	if args.hstddev:
		config.hstddev = args.hstddev
	if args.hplusminus:
		config.hplusminus = args.hplusminus
	
	# Evo Params
	if args.mutation:
		if args.mutation > 1 or args.mutation < 0:
			msg = "%f is not in the range [0,1]" % (args.mutation)
			raise argparse.ArgumentTypeError(msg)
		config.mutation = args.mutation
	if args.populationsize:
		config.populationsize = args.populationsize
	if args.generations:
		config.generations = args.generations
	if args.epochs:
		config.epochs = args.epochs
	
	# System Params
	if args.batchsize:
		config.batchsize
	if args.autobatch:
		config.autobatch
	if config.autobatch:
		train_size = training['features'].shape[0]
		config.batchsize = int(train_size*0.8/config.populationsize)
	if args.minibatch:
		config.minibatch
	if args.finaltrain:
		config.finaltrain
	if args.verbose:
		global DEBUG
		EvoAE.DEBUG = True
	
	# Run Params
	if args.output:
		config.output = args.output
	if args.name:
		config.name = args.name

	uid = datetime.now().strftime('%Y-%m-%d %H-%M-%S')

	#config.output = "%s p%d g%d e%d %s/" % (config.output,config.populationsize,config.generations,config.epochs,uid)
	config.output = "%s/" % (config.output)

	# Make sure output folder is initialized
	if not os.path.isdir(config.output):
		os.makedirs(config.output)

	f = open('%s%s.conifg' % (config.output,config.name),'w')
	EvoConfig.encode(config,f)
	f.close()

	if args.normalize:
		# Normalize by feature
		norm = {}
		norm['mean'] = normalize.mean(training['features'])
		norm['std'] = normalize.std(training['features'])

		training['features'] = normalize.normalize(training['features'],norm['mean'],norm['std'])
		
		norm['min'] = normalize.min(training['features'])
		norm['range'] = normalize.range(training['features'])

		training['features'] = normalize.scale(training['features'],norm['min'],norm['range'])
		
		f = open("%s%s.normalize.json" % (config.output,config.name), 'w')
		normalize.encode(norm,f)
		f.close()

	config.insize = insize

	return (training,config)

if __name__ == '__main__':
	main()