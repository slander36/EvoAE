'''

AutoEncoder - Auto Encoder base 

@author Sean Lander
@copyright 2014
@title AutoEncoder

Each Auto Encoder has the ability to take a sample
and rebuild it. Auto Encoders are a collection
of weights and biases.

'''

from numpy import *

from utils import *


class AutoEncoder:
	
	DEBUG = False

	def __init__(self, Win=None, bin=None, Wout=None, bout=None, config=None, id=0, debug=False):
		global DEBUG
		DEBUG = debug

		self.id = id

		self.Win = Win
		self.bin = bin
		self.Wout = Wout
		self.bout = bout

		self.config = config
		if not self.config:
			raise BaseException("No config provided")

	def calculateActivation(self,x,w,b):
		# Calculate W*x+b
		z = dot(w,x)+b

		# Calculate activation
		a = activation.sigmoid(z)

		return (z,a)

	def getWeights(self):
		# Fill in stuff here
		# Do nothing, Base AutoEncoder holds its weights
		# directly, not in objects
		return True

	def calculateHidden(self,data):
		self.getWeights()
		(zin,ain) = self.calculateActivation(data,self.Win,self.bin)
		return (zin,ain)

	def calculateOutput(self,ain):
		self.getWeights()
		(zout,aout) = self.calculateActivation(ain,self.Wout,self.bout)
		return (zout,aout)

	def feedforward(self,data):
		# print("Feed forward")
		self.getWeights()

		# print("Calculate Hidden Activation")
		(zin,ain) = self.calculateHidden(data)

		# print("Calculate Output Activation")
		(zout,aout) = self.calculateOutput(ain)

		return (zin, ain, zout, aout)

	def backprop(self,x,alpha,zin,ain,zout,aout,error,o_Dwin=0,o_Dbin=0,o_Dwout=0,o_Dbout=0):
		# Grab the current weights and biases
		self.getWeights()

		# For each a(x)i in a(x) calculate d(x)i
			# d(x)i = -(yi-a(x)i)*f'(z(x)i)

		# For each a(h)i in a(h) calculate d(h)i
			# d(h)i = sumj(w(h)Tji*d(x)j)*f'(z(h)i)

		# Compute partial derivatives
			# gw(l)ijJ(wb;xy) = a(l)j*d(l+1)i 	-> a(h)j*d(x)i
			# gb(l)i J(wb;xy) = d(l+1)i 		-> d(x)i

		# For i in inputs compute gw, gb and updated dw, db
			# dw += gw
			# db += gb
		
		# Or in vector matrix form
			# d(x) = -(y-a(x))f'(z(x))
			# d(h) = [w(h)'d(x)]*f'(z(h))
			# gw(l)J() = d(l+1)*(a(l))'
			# gb(l)J() = d(l+1)*ones(s(l))'

		# print("Calculate delta x")
		dout = -error*activation.dsigmoid_post(aout)

		# print("Calculate delta h")
		din = dot(transpose(self.Wout),dout)*activation.dsigmoid_post(ain)

		# print("Compute gradients and updates")
		gwout = dot(dout,transpose(ain))

		gbout = sum(dout,axis=1)
		gbout = gbout.reshape((gbout.shape[0],1))

		gwin = dot(din,transpose(x))

		gbin = sum(din,axis=1)
		gbin = gbin.reshape((gbin.shape[0],1))

		# print("Update weights and biases")
		# Update w, b
		m = x.shape[0]
		momentum = self.config.momentum
		lamb = self.config.lamb

		# Update the weights
			# DW = -alpha([1/m]GW+lamb*GW)
			# DB = -alpha([1/m]*GB)
			# w' = w + DW
			# b' = b + DB

		Dwin = -alpha*((1/m)*gwin+lamb*gwin)
		Dbin = -alpha*((1/m)*gbin)
		Dwout = -alpha*((1/m)*gwout+lamb*gwout)
		Dbout = -alpha*((1/m)*gbout)

		self.Win += Dwin + momentum*o_Dwin
		self.bin += Dbin + momentum*o_Dbin
		self.Wout += Dwout + momentum*o_Dwout
		self.bout += Dbout + momentum*o_Dbout

		return (Dwin,Dbin,Dwout,Dbout)