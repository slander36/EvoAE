'''
EvoAE.py output parser
'''

import json
import argparse
import os
import platform
import subprocess

import numpy as np
import matplotlib.pyplot as plt

def main(datasize,directory,names):
	data = {}
	datanames = names
	#datanames = ['wine','iris','heartdisease','mnist']
	for dataname in datanames:
		data[dataname] = {}
		for root, dirs, files in os.walk(directory):
			for name in files:
				if datasize in root and "accuracies" in name and dataname in root:
					# Get the filename
					filename = "%s/%s" % (root,name)
					# And the run type (Full Data, Batchsize, etc.)
					'''WARNING - THIS IS WINDOWS SPECIFIC. CHANGE TO YOUR OS' PATH TYPE'''
					runtype = root.split('/')[4].split('\\')[0]

					# If this entry doesn't exist, initialize it
					if runtype not in data[dataname].keys():
						data[dataname][runtype] = {}
						data[dataname][runtype]['accuracies'] = []
						data[dataname][runtype]['acc_best_training'] = []
						data[dataname][runtype]['acc_best_error'] = []
						data[dataname][runtype]['errors'] = []
						data[dataname][runtype]['err_best_error'] = []
						data[dataname][runtype]['times'] = {'avg':0,'std':0}
					
					# Open the acuracy file and read it
					f = open(filename)
					rundata = json.load(f)

					# Initialize some variables to watch for
					testing_accuracies = []
					errors = []
					acc_best_training = 0
					acc_best_error = 1e10
					err_best_error = 1e10
					# Loop through the data collecting sweet information
					for _,v in rundata['evoae'].items():
						training = v['training']
						testing = v['testing']
						error = v['sserror']
						# Update the values for best training and best error
						if training > acc_best_training:
							acc_best_training = testing
						if error < acc_best_error:
							acc_best_error = testing
							err_best_error = error
						testing_accuracies.append(testing)
						errors.append(error)
					f.close()
					data[dataname][runtype]['accuracies'] += testing_accuracies
					data[dataname][runtype]['acc_best_training'].append(acc_best_training)
					data[dataname][runtype]['acc_best_error'].append(acc_best_error)
					data[dataname][runtype]['errors'] += errors
					data[dataname][runtype]['err_best_error'].append(err_best_error)
					data[dataname][runtype]['baseline'] = rundata['baseline']['testing']

	for root, dirs, files in os.walk(directory):
		for name in files:
			if datasize in root and "txt" in name:
				# Get the filename
				filename = "%s/%s" % (root,name)
				# And the run type (Full Data, Batchsize, etc.)
				'''WARNING - THIS IS WINDOWS SPECIFIC. CHANGE TO YOUR OS' PATH TYPE'''
				runtype = root.split('/')[4].split('\\')[0]

				# Open the acuracy file and read it
				f = open(filename)

				if 'MNIST' not in name and 'profile' not in name:
					curdataname = None # Default to no name
					for line in f:
						if 'WINE' in line:
							curdataname = 'wine'
							continue
						if 'IRIS' in line:
							curdataname = 'iris'
							continue
						if 'HEART DISEASE' in line:
							curdataname = 'heartdisease'
							continue
						if 'MNIST' in line:
							curdataname = 'mnist'
							continue
						if curdataname is None or curdataname not in datanames:
							continue
						if 'Runtime' in line:
							line = line.split(':')
							if 'Avg' in line[0]:
								data[curdataname][runtype]['times']['avg'] = float(line[1].strip())
							if 'Std' in line[0]:
								data[curdataname][runtype]['times']['std'] = float(line[1].strip())
				f.close()

	# For each dataname, add its info to the graph
	width = 0.5
	for dataname,v1 in data.items():
		baseline = []
		accus = []
		acc_best_training = []
		acc_best_erros = []
		erros = []
		err_best_error = []
		times = {'avg':[],'std':[]}
		labels = []
		v1 = sorted(v1.items(), key=lambda x: x[0])

		# Create the figure
		fig = plt.figure(dataname,figsize=(16,9))
		accu = fig.add_subplot(2,3,1)
		time = fig.add_subplot(2,3,2)
		erro = fig.add_subplot(2,3,3)
		accu_best_training = fig.add_subplot(2,3,4)
		accu_best_error = fig.add_subplot(2,3,5)
		best_erro = fig.add_subplot(2,3,6)

		for runtype,v2 in v1:
			labels.append(runtype)
			baseline.append(v2['baseline'])
			accus.append(v2['accuracies'])
			acc_best_training.append(v2['acc_best_training'])
			acc_best_erros.append(v2['acc_best_error'])
			erros.append(v2['errors'])
			err_best_error.append(v2['err_best_error'])
			times['avg'].append(v2['times']['avg'])
			times['std'].append(v2['times']['std'])
		
		pos = np.array(range(len(v1)))
		
		tbaseline = sum(baseline)/len(baseline)
		for i in pos:
			baseline[i] = tbaseline

		min_accu = min(min(accus))
		max_accu = max(max(accus))
		accu_range = max_accu - min_accu
		
		accubp = accu.boxplot(accus,positions=pos)
		acculine = accu.plot(baseline,color='orange')
		accu.set_xticklabels(labels,rotation='vertical')
		accu.set_title("Overall Testing Acc.")
		accu.set_ylim(min_accu - accu_range*0.1,max_accu + accu_range*0.1)

		accu_best_trainingbp = accu_best_training.boxplot(acc_best_training,positions=pos)
		acculine = accu_best_training.plot(baseline,color='orange')
		accu_best_training.set_xticklabels(labels,rotation='vertical')
		accu_best_training.set_title("Testing Acc. of Best Training Acc.")
		accu_best_training.set_ylim(min_accu - accu_range*0.1,max_accu + accu_range*0.1)

		accu_best_errorbp = accu_best_error.boxplot(acc_best_erros,positions=pos)
		acculine = accu_best_error.plot(baseline,color='orange')
		accu_best_error.set_xticklabels(labels,rotation='vertical')
		accu_best_error.set_title("Testing Acc. of Best Error")
		accu_best_error.set_ylim(min_accu - accu_range*0.1,max_accu + accu_range*0.1)

		timebp = time.bar(pos,times['avg'],width,color='y',yerr=times['std'])
		time.set_xticks(pos+width/2)
		time.set_xticklabels(labels,rotation='vertical')
		time.set_title("Time to Train")

		min_erro = min(min(erros))
		max_erro = max(max(erros))
		erro_range = max_erro - min_erro

		errobp = erro.boxplot(erros,positions=pos)
		erro.set_xticklabels(labels,rotation='vertical')
		erro.set_title("Overall Reconstruction Error")
		erro.set_ylim(min_erro - erro_range*0.1,max_erro + erro_range*0.1)

		best_errobp = best_erro.boxplot(err_best_error,positions=pos)
		best_erro.set_xticklabels(labels,rotation='vertical')
		best_erro.set_title("Best Reconstruction Error")
		best_erro.set_ylim(min_erro - erro_range*0.1,max_erro + erro_range*0.1)

		plt.tight_layout()
		#plt.show()
		plt.savefig("%s/%s-%s.png" % (directory,datasize,dataname), dpi=96, format='png')

	'''
	NOTE: This has not been tested outside of Windows environment yet!
	TODO: Test Linux and OSX environments to see if these are the correct
	calls to use to open different file browsers
	'''
	_SYSTEM = platform.system()
	if _SYSTEM == "Windows":
		subprocess.Popen(r'explorer "%s"' % (os.path.abspath(directory)))
	if _SYSTEM == "Linux":
		subprocess.Popen(r'natilus %s/' % (os.path.abspath(directory)))
	if _SYSTEM == "Darwin":
		subprocess.Popen(r'open %s/' % (os.path.abspath(directory)))

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('size')
	parser.add_argument('dir')
	parser.add_argument('names',nargs='*')
	args = parser.parse_args()
	main(args.size,args.dir,args.names)