'''

activations.py - Activation functions and their derivatives

@author Sean Lander
@copyright 2014
@title Activations

'''

import numpy as np
from numpy import exp, where
import json

class activation:

	def sigmoid(z,c=1):
		# Fit to range to ward off out of bounds exceptions from exp()
		tz = where(z < -200, -200.0, z)
		tz = where(z > 200, 200.0, tz)
		return 1 / (1 + exp(-c*tz))

	def dsigmoid_post(a):
		return a*(1-a)

	def dsigmoid_pre(z,c=1):
		f = sigmoid(z,c)
		return dsigmoid_post(f)

	def htan(z,c=1):
		# Fit to range to ward off out of bounds exceptions from exp()
		tz = where(z < -200, -200.0, z)
		tz = where(z > 200, 200.0, tz)
		return (exp(c*tz)-exp(-c*z))/(exp(c*z)+exp(-c*z))

	def dhtan_post(a):
		return 1 - a**2

	def dhtan_pre(z,c=1):
		f = htan(z,c)
		return dhtan_post(f)

class normalize:

	def mean(data,axis=0):
		return np.mean(data,axis=axis)

	def std(data,axis=0):
		return np.std(data,axis=axis)

	def normalize(data,mean,std):
		return (data - mean) / std

	def min(data,axis=0):
		return data.min(axis=axis)

	def range(data,axis=0):
		return data.max(axis=axis)-data.min(axis=axis)

	def scale(data,smin,srange):
		return (data - smin) / srange

	def encode(norm,fp):
		tmp = {}
		tmp['mean'] = norm['mean'].tolist()
		tmp['std'] = norm['std'].tolist()
		tmp['min'] = norm['min'].tolist()
		tmp['range'] = norm['range'].tolist()
		json.dump(tmp,fp)

	def decode(fp):
		norm = json.load(fp)
		norm['mean'] = np.array(norm['mean'])
		norm['std'] = np.array(norm['std'])
		norm['min'] = np.array(norm['min'])
		norm['range'] = np.array(norm['range'])
		return norm