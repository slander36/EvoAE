'''

Evo AE - Evolutionay AutoEncoder for Deep Learning Networks

@author Sean Lander
@copyright 2014
@title EvoAE

Training and Testing data should be a csv file with the first
column containing the label (number or string), with the
following columns containing the feature data.

Adding the -normalize flag will cause all data to be normalized
using its z-score.

Training data will be used to build an EvoAE population
using meta-parameters found in the configuration file (config.json)

'''

import os
import sys
import argparse
import platform
import json
from datetime import datetime

import time

import numpy as np
from sklearn import datasets, svm, metrics
import matplotlib.pyplot as plt

from EvoAutoEncoder import EvoAutoEncoder
from EvoConfig import EvoConfig
from utils import normalize

DEBUG = False

def main(training,validation,config):
	global DEBUG
	if DEBUG:
		print("Running training cycle")
	aepop = []
	sserrors = []
	validation_sserrors = []
	rankings = {}
	validation_sserror = 0

	encoders_range = np.arange(0,config.populationsize)
	generations_range = np.arange(1,config.generations+1)
	epochs_range = np.arange(0,config.epochs)
	
	# Initialize all Auto Encoders
	for i in encoders_range:
		aepop.append(EvoAutoEncoder(config=config,gen=0,id=i,debug=DEBUG))
		sserrors.append([])
		validation_sserrors.append([])
		if DEBUG:
			print("Created AE %d(%d)[%d]" % (i,aepop[i].size,aepop[i].id))

	# Check for minibatch or batchsize
	minibatch = config.minibatch
	batchsize = True if config.batchsize is not None else False
	batches = 0
	if batchsize:
		# User-specified batch size
		batches = int(training.shape[1] / config.batchsize)
		training_data_batches = np.array_split(training,batches,axis=1)
	elif minibatch:
		# Number of bathes based on population size
		training_data_batches = np.array_split(training,config.populationsize,axis=1)

	# If we're running with minibatches, then pre-train the nodes on smaller
	# data sets before training on the whole set
	if DEBUG:
		print(":::Training Initial Generation:::")
	# Do search step of Memetic
	for i in encoders_range:
		last_error = 1e10
		if minibatch:
			# Run this AE's mini-batches
			(sserror,last_error) = aepop[i].search(training_data_batches[i])
		elif batchsize:
			# Run on first batch
			(sserror,last_error) = aepop[i].search(training_data_batches[0])
		else:
			# Run using whole data set
			(sserror,last_error) = aepop[i].search(training)
		
		# Update running error
		sserrors[i] += sserror

		# Calculate validation error
		(_,_,_,aout) = aepop[i].feedforward(validation)
		validation_error = (validation - aout)
		validation_sserror += np.average(np.sum(validation_error**2,axis=0))
		validation_sserrors[i] += [validation_sserror]

		# Rank based on training error
		rankings[i] = last_error

	# Initialize validation
	best_validation_sserror = validation_sserror
	best_pop = aepop
	best_rankings = rankings
	validation_count = 0

	# Run Local Search on each AE
	for g in generations_range:
		if DEBUG:
			print(":::Creating Generation %d:::" % (g))
		
		# Sort by error
		rankings = np.array(sorted(rankings.items(), key=lambda x : x[1]))
		# total_error = np.sum(rankings,axis=0)[1]
		rankings_range = rankings[:,1].max() - rankings[:,1].min()

		# Select top candidates for crossover
		# Low error is better, so need to invert selection probability
		#	scale to 0 and 1
		# 	subtract 1
		#	take the absolute value
		#	give a probability based on score/sum(scores)
		prob = (rankings[:,1] - rankings[:,1].min())/rankings_range
		prob = abs(1-prob)/np.sum(prob)

		# Select parents to cross until encoders are full
		i = 0
		while i+1 < config.populationsize:
			# Randomly select two parents
			p1 = p2 = None
			while not p1:
				p1 = np.random.randint(config.populationsize)
				# If rand is less than prob, accept, otherwise reject
				# In this case, if rand is greater than prob, reject
				if prob[p1] < np.random.rand():
					p1 = None
				else:
					p1 = int(rankings[p1,0])
			while not p2:
				p2 = np.random.randint(config.populationsize)
				# If rand is less than prob, accept, otherwise reject
				# In this case, if rand is greater than prob, reject
				# Also, if p1 is p2, select a different parent
				if p1 == p2 or prob[p2] < np.random.rand():
					p2 = None
				else:
					p2 = int(rankings[p2,0])
			if DEBUG:
				print("Crossing %d(%d) and %d(%d) for %d and %d" % (p1,aepop[p1].size,p2,aepop[p2].size,i,i+1))
			p1 = aepop[p1]
			p2 = aepop[p2]
			(f1,f2) = EvoAutoEncoder.crossover(p1,p2)
			aepop[i] = EvoAutoEncoder(features=f1,config=config,gen=g,id=i,debug=DEBUG)
			aepop[i+1] = EvoAutoEncoder(features=f2,config=config,gen=g,id=i+1,debug=DEBUG)
			i += 2
		if i < config.populationsize:
			# Randomly select two parents
			p1 = p2 = None
			while not p1:
				p1 = np.random.randint(config.populationsize)
				# If rand is less than prob, accept, otherwise reject
				# In this case, if rand is greater than prob, reject
				if prob[p1] < np.random.rand():
					p1 = None
				else:
					p1 = int(rankings[p1,0])
			while not p2:
				p2 = np.random.randint(config.populationsize)
				# If rand is less than prob, accept, otherwise reject
				# In this case, if rand is greater than prob, reject
				# Also, if p1 is p2, select a different parent
				if p1 == p2 or prob[p2] < np.random.rand():
					p2 = None
				else:
					p2 = int(rankings[p2,0])
			if DEBUG:
				print("Crossing %d(%d) and %d(%d) for %d and %d" % (p1,aepop[p1].size,p2,aepop[p2].size,i,i+1))
			p1 = aepop[p1]
			p2 = aepop[p2]
			(f1,f2) = EvoAutoEncoder.crossover(p1,p2)
			aepop[i] = EvoAutoEncoder(features=f1,config=config,gen=g,id=i,debug=DEBUG)

		if DEBUG:
			print(":::Mutating Generation %d:::" % (g))
		for ae in aepop:
			# Move these lines into their own methods

			# Random AE
			mutation_i = np.random.randint(len(aepop))
			mutation = aepop[mutation_i]

			# Random Feature
			mutation = mutation.randomFeature()
			ae.mutate(mutation)
		
		# Using for benchmark. Remove later
		for i in encoders_range:
			aepop[i].gen = g
		# End remove

		if DEBUG:
			print(":::Training Generation %d:::" % (g))
		# Do search step of Memetic
		rankings = {}
		validation_sserror = 0
		for i in encoders_range:
			last_error = 1e10
			if minibatch:
				# Run this AE's mini-batches
				(sserror,last_error) = aepop[i].search(training_data_batches[i])
			elif batchsize:
				# Run on consecutive looping batches
				(sserror,last_error) = aepop[i].search(training_data_batches[g%batches])
			else:
				# Run using whole data set
				(sserror,last_error) = aepop[i].search(training)
			
			# Update running error
			sserrors[i] += sserror

			# Calculate validation error
			(_,_,_,aout) = aepop[i].feedforward(validation)
			validation_error = (validation - aout)
			validation_sserror += np.average(np.sum(validation_error**2,axis=0))
			validation_sserrors[i] += [validation_sserror]

			# Rank based on training error
			rankings[i] = last_error # Based on training
			# rankings[i] = validation_sserror # Based on validation
			'''
			Potential change to ranking:
				last_error * (input_size+aepop.size) / input_size
			This will scale error to favor a lower number of hidden units
			May reduce accuracy, but should help favor lower hidden size execpt
			in cases where a larger size is vastly superior
			'''

		if validation_sserror < best_validation_sserror:
			best_validation_sserror = validation_sserror
			best_pop = aepop
			best_rankings = rankings
			validation_count = 0
		elif validation_count < 3:
			validation_count += 1
		else:
			rankings = best_rankings
			break
	
	if config.finaltrain != 'None':
		if DEBUG:
			print(":::Final Training on Best Population:::")
		# Do search step of Memetic
		rankings = {}
		validation_sserror = 0
		for i in encoders_range:
			last_error = 1e10
			if config.finaltrain == 'Batch':
				# Run through all mini or data-batches
				for training_data_batch in training_data_batches:
					(sserror,last_error) = best_pop[i].search(training_data_batch)
					# Update running error
					sserrors[i] += sserror
			else:
				(sserror,last_error) = best_pop[i].search(training)
				# Update running error
				sserrors[i] += sserror

			# Calculate validation error
			(_,_,_,aout) = best_pop[i].feedforward(validation)
			validation_error = (validation - aout)
			validation_sserror += np.average(np.sum(validation_error**2,axis=0))
			validation_sserrors[i] += [validation_sserror]

			# Rank based on training error
			rankings[i] = last_error # Based on training
	
	return (best_pop,rankings,sserrors)

def rankClassifyPlot(training,testing,best_pop,rankings,sserrors,config):
	'''
	Get Training and Testing - Data and labels
	'''
	training_data = training['features'].T
	training_labels = training['labels'].T
	testing_data = testing['features'].T
	testing_labels = testing['labels'].T

	'''
	other params
	'''
	encoders_range = np.arange(0,config.populationsize)

	'''
	Storing Weights
	'''
	# Sort by error
	rankings = sorted(rankings.items(), key=lambda x : x[1])
	rank = 0
	for i,_ in rankings:
		rank += 1
		f = open('%s%d-AE%d.weights.json' % (config.output,rank,i),'w')
		EvoAutoEncoder.encode(best_pop[i],f)
		f.close()

	'''
	Classification
	'''
	classification = {}
	classification['baseline'] = {}
	classification['evoae'] = {}

	'''
	SVM Classification
	'''

	# Create an SVM and traing it using raw input
	clf = svm.LinearSVC()
	X = training_data.T
	y = training_labels
	clf.fit(X,y)

	'''
	Running SVM with raw feature set
	'''

	raw_training_accuracies = []
	raw_testing_accuracies = []

	# Get training accuracy of SVM
	training_predicted_labels = clf.predict(X)
	training_correct_labels = np.where(training_labels==training_predicted_labels,1,0)
	raw_training_accuracy = 100*sum(training_correct_labels)/training_labels.shape[0]
	print("raw : training accuracy : %.2f%%" % (raw_training_accuracy))
	for i in encoders_range:
		raw_training_accuracies.append(raw_training_accuracy)
	raw_training_accuracies = np.array(raw_training_accuracies)

	# Run SVM against testing data
	X = testing_data.T
	testing_predicted_labels = clf.predict(X)
	testing_correct_labels = np.where(testing_labels==testing_predicted_labels,1,0)
	raw_testing_accuracy = 100*sum(testing_correct_labels)/testing_labels.shape[0]
	print("raw : testing accuracy : %.2f%%" % (raw_testing_accuracy))
	for i in encoders_range:
		raw_testing_accuracies.append(raw_testing_accuracy)
	raw_testing_accuracies = np.array(raw_testing_accuracies)

	# Store the accuracies
	accuracies = {}
	accuracies['training'] = raw_training_accuracy
	accuracies['testing'] = raw_testing_accuracy

	classification['baseline'] = accuracies

	'''
	Running SVM with reduced feature set generated by AutoEncoder
	'''

	reduced_training_accuracies = []
	reduced_testing_accuracies = []
	
	rank = 0
	for i,sserror in rankings:
		rank += 1
		# Create an SVM and traing it using hidden layer
		# activation and and training data labels
		clf = svm.LinearSVC()
		(_,ain) = best_pop[i].calculateHidden(training_data)
		X = ain.T
		y = training_labels
		clf.fit(X,y)

		# Get training accuracy of SVM
		training_predicted_labels = clf.predict(X)
		training_correct_labels = np.where(training_labels==training_predicted_labels,1,0)
		training_accuracy = 100*sum(training_correct_labels)/training_labels.shape[0]
		print("%d reduced : training accuracy : %.2f%%" % (i,training_accuracy))
		reduced_training_accuracies.append(training_accuracy)
		
		# Run AE against testing data and use trained SVM
		# against generated features, then compare to expected
		# testing labels
		(_,test_ain) = best_pop[i].calculateHidden(testing_data)
		X = test_ain.T
		testing_predicted_labels = clf.predict(X)
		testing_correct_labels = np.where(testing_labels==testing_predicted_labels,1,0)
		testing_accuracy = 100*sum(testing_correct_labels)/testing_labels.shape[0]
		print("%d reduced : testing accuracy : %.2f%%" % (i,testing_accuracy))
		reduced_testing_accuracies.append(testing_accuracy)

		# Store the accuracies
		accuracies = {}
		accuracies['training'] = training_accuracy
		accuracies['testing'] = testing_accuracy
		accuracies['sserror'] = sserror
		accuracies['hiddensize'] = best_pop[i].size

		classification['evoae']['%d-AE%d' % (rank,i)] = accuracies

	reduced_training_accuracies = np.array(reduced_training_accuracies)
	reduced_testing_accuracies = np.array(reduced_testing_accuracies)
	
	f = open('%s%s.accuracies.json' % (config.output,config.name),'w')
	json.dump(classification,f)
	f.close()

	plt.clf()
	fig = plt.figure(config.name,figsize=(9,16))
	fig.subplots_adjust(right=0.75)

	subp1 = fig.add_subplot(311)
	for i in encoders_range:
		subp1.plot(range(0,len(sserrors[i])),sserrors[i])
	subp1.set_ylabel('Avg Sum Squared Error')
	subp1.set_xlabel('Iteration')
	
	subp2ax1 = fig.add_subplot(312)
	subp2ax2 = subp2ax1.twinx()

	subp3ax1 = fig.add_subplot(313)
	subp3ax2 = subp3ax1.twinx()

	width = 0.2

	rederrs = []
	for _,err in rankings:
		rederrs.append(err)
	rederr = subp2ax2.bar(encoders_range+width+width,rederrs,width,color='b')

	rawtrainacc = subp2ax1.plot(encoders_range,raw_training_accuracies,'g-',lw=2,zorder=5)#,label='Raw Features Training')
	rawtestacc = subp2ax1.plot(encoders_range,raw_testing_accuracies,'r-',lw=2,zorder=5)#,label='Raw Features Testing')
	
	redtrainacc = subp2ax1.bar(encoders_range+width,reduced_training_accuracies,width,color='y',zorder=6)#,label='AE Features Training')
	redtestacc = subp2ax1.bar(encoders_range,reduced_testing_accuracies,width,color='m',zorder=6)#,label='AE Features Testing')
	
	ymins = np.array([raw_training_accuracies.min(),raw_testing_accuracies.min(),reduced_training_accuracies.min(),reduced_testing_accuracies.min()])
	ymaxs = np.array([raw_training_accuracies.max(),raw_testing_accuracies.max(),reduced_training_accuracies.max(),reduced_testing_accuracies.max()])
	yrange = ymaxs.max() - ymins.min()
	subp2ax1.set_ylim(ymins.min()-yrange*0.1,ymaxs.max()+yrange*0.1)

	subp2ax1.set_ylabel('Accuracy %%')
	subp2ax2.set_ylabel('SSError')
	subp2ax1.set_xlabel('Auto Encoder')

	rederr = subp3ax1.bar(encoders_range,rederrs,width,color='b')

	redhsizes = []
	for i,_ in rankings:
		redhsizes.append(best_pop[i].size)
	redhsize = subp3ax2.bar(encoders_range+width,redhsizes,width,color='c')

	subp3ax1.set_ylabel('SSError')
	subp3ax2.set_ylabel('Hidden Size')
	subp3ax1.set_xlabel('Auto Encoder')

	fig.legend((rawtrainacc[0],rawtestacc[0],redtrainacc[0],redtestacc[0],rederr[0],redhsize[0]),('Raw Features Training','Raw Features Testing','AE Features Training','AE Features Testing','AE SSError','AE Hidden Size'))

	plt.tight_layout()

	plt.savefig("%s%s.png" % (config.output,config.name), dpi=96, format='png')

	# Reenable if you want it to pause between runs
	# plt.show()

	'''
	NOTE: This has not been tested outside of Windows environment yet!
	TODO: Test Linux and OSX environments to see if these are the correct
	calls to use to open different file browsers
	'''
	'''
	_SYSTEM = platform.system()
	if _SYSTEM == "Windows":
		subprocess.Popen(r'explorer "%s"' % (os.path.abspath(config.output)))
	if _SYSTEM == "Linux":
		subprocess.Popen(r'natilus %s/' % (os.path.abspath(config.output)))
	if _SYSTEM == "Darwin":
		subprocess.Popen(r'open %s/' % (os.path.abspath(config.output)))
	'''


def mnist_example(train_size,test_size):
	from mnist import read as readmnist
	train_imgs, train_lbls = readmnist(range(10),'training')
	sample_count = train_imgs.shape[0]
	feature_count = train_imgs.shape[1]*train_imgs.shape[2]
	training = {}
	training['features'] = train_imgs.reshape(sample_count,feature_count)
	training['labels'] = train_lbls.reshape(sample_count)

	test_imgs, test_lbls = readmnist(range(10),'testing')
	sample_count = test_imgs.shape[0]
	feature_count = test_imgs.shape[1]*test_imgs.shape[2]
	testing = {}
	testing['features'] = test_imgs.reshape(sample_count,feature_count)
	testing['labels'] = test_lbls.reshape(sample_count)

	if training['features'].shape[1] != testing['features'].shape[1]:
		raise BaseException('Number of features imblanaced between training and testing set in MNIST')

	training['features'] = training['features'][0:train_size]
	training['labels'] = training['labels'][0:train_size]
	testing['features'] = testing['features'][0:test_size]
	testing['labels'] = testing['labels'][0:test_size]

	# Normalize by sample
	norm = {}

	norm['training'] = {}
	norm['testing'] = {}

	training['features'] = training['features'].T
	testing['features'] = testing['features'].T

	norm['training']['mean'] = normalize.mean(training['features'])
	norm['training']['std'] = normalize.std(training['features'])

	norm['testing']['mean'] = normalize.mean(testing['features'])
	norm['testing']['std'] = normalize.std(testing['features'])

	training['features'] = normalize.normalize(training['features'],norm['training']['mean'],norm['training']['std'])

	testing['features'] = normalize.normalize(testing['features'],norm['testing']['mean'],norm['testing']['std'])

	norm['training']['min'] = normalize.min(training['features'])
	norm['training']['range'] = normalize.range(training['features'])

	norm['testing']['min'] = normalize.min(testing['features'])
	norm['testing']['range'] = normalize.range(testing['features'])

	training['features'] = normalize.scale(training['features'],norm['training']['min'],norm['training']['range'])

	testing['features'] = normalize.scale(testing['features'],norm['testing']['min'],norm['testing']['range'])

	training['features'] = training['features'].T
	testing['features'] = testing['features'].T

	return (training,testing)

def parseInput():
	parser = argparse.ArgumentParser(description="Runner for Evolutionary AutoEncoder training and testing")

	data_group = parser.add_argument_group('Data')
	data_group.add_argument("training",type=argparse.FileType(),help="data for training")
	data_group.add_argument("testing",type=argparse.FileType(),help="data for testing")
	
	config_group = parser.add_argument_group('Configuration')
	config_group.add_argument("--mnist",help="use the MNIST data set instead",action='store_true')
	config_group.add_argument("--mnisttrain",help="number of training samples for mnist",type=int,default=6000)
	config_group.add_argument("--mnisttest",help="number of testing samples for mnist",type=int,default=1000)
	config_group.add_argument("-a","--alpha",help="learning rate",type=float)
	config_group.add_argument("-m","--momentum",help="momentum term for congugate gradient descence (0 to turn off)",type=float)
	config_group.add_argument("-l","--lamb",help="lambda term for weight decay (0 to turn off)",type=float)
	config_group.add_argument("-s","--hsize",help="size of the hidden layer",type=int)
	config_group.add_argument("-d","--hstddev",help="stddev of hidden layer size on initialization",type=float)
	config_group.add_argument("-+","--hplusminus",help="+/- of hidden layer size on initialization",type=int)
	config_group.add_argument("-u","--mutation",help="mutation rate [0,1] scale",type=float)
	config_group.add_argument("-p","--populationsize",help="number of organisms in the population")
	config_group.add_argument("-g","--generations",help="number of generations")
	config_group.add_argument("-e","--epochs",help="numer of search iterations")
	config_group.add_argument("--batchsize",help="the size of each batch used in training (optional)",type=int)
	config_group.add_argument("--autobatch",help="splits data into populationsize many batches (optional)",action='store_true')
	config_group.add_argument("--minibatch",help="splits data into populationsize many batches, with each member training against a single batch (optional)",action='store_true')
	config_group.add_argument("--finaltrain",help="specify if you want a final round of training after convergence and which type (optional)",choices=['All','Batch','None'],default='None')
	config_group.add_argument("-n","--normalize",help="flag for if data needs to be normalized before training",action='store_true')
	config_group.add_argument("-o","--output",help="output directory name")
	config_group.add_argument("-t","--name",help="name of experiment")
	config_group.add_argument("-v","--verbose",help="verbose output",action='store_true')

	parser.add_argument("-c","--config",type=argparse.FileType('r'),help="configuration file")

	args = parser.parse_args()

	if args.mnist:
		(training,testing) = mnist_example(args.mnisttrain,args.mnisttest)
	else:
		training = np.loadtxt(args.training,delimiter=',')
		testing = np.loadtxt(args.testing,delimiter=',')

		training = {'labels':training[:,0],'features':training[:,1:]}
		testing = {'labels':testing[:,0],'features':testing[:,1:]}

	insize = training['features'].shape[1]
	if insize != testing['features'].shape[1]:
		raise BaseException("Training and Testing need to have the same number of features: {0} {1}".format(training.shape,testing.shape))

	if args.config:
		config = EvoConfig(config=args.config)
	else:
		config = EvoConfig()

	# Training Params
	if args.alpha:
		config.alpha = args.alpha
	if args.momentum:
		config.momentum = args.momentum
	if args.lamb:
		config.lamb = args.lamb
	
	# Structure Params
	if args.hsize:
		config.hsize = args.hsize
	if args.hstddev:
		config.hstddev = args.hstddev
	if args.hplusminus:
		config.hplusminus = args.hplusminus
	
	# Evo Params
	if args.mutation:
		if args.mutation > 1 or args.mutation < 0:
			msg = "%f is not in the range [0,1]" % (args.mutation)
			raise argparse.ArgumentTypeError(msg)
		config.mutation = args.mutation
	if args.populationsize:
		config.populationsize = args.populationsize
	if args.generations:
		config.generations = args.generations
	if args.epochs:
		config.epochs = args.epochs
	
	# System Params
	if args.batchsize:
		config.batchsize
	if args.autobatch:
		config.autobatch
	if config.autobatch:
		train_size = training['features'].shape[0]
		config.batchsize = int(train_size*0.8/config.populationsize)
	if args.minibatch:
		config.minibatch
	if args.finaltrain:
		config.finaltrain
	if args.verbose:
		global DEBUG
		DEBUG = True
	
	# Run Params
	if args.output:
		config.output = args.output
	if args.name:
		config.name = args.name

	uid = datetime.now().strftime('%Y-%m-%d %H-%M-%S')

	config.output = "%s p%d g%d e%d %s/" % (config.output,config.populationsize,config.generations,config.epochs,uid)

	# Make sure output folder is initialized
	if not os.path.isdir(config.output):
		os.makedirs(config.output)

	f = open('%s%s.conifg' % (config.output,config.name),'w')
	EvoConfig.encode(config,f)
	f.close()

	if args.normalize:
		# Normalize by feature
		norm = {}
		norm['mean'] = normalize.mean(training['features'])
		norm['std'] = normalize.std(training['features'])

		training['features'] = normalize.normalize(training['features'],norm['mean'],norm['std'])
		testing['features'] = normalize.normalize(testing['features'],norm['mean'],norm['std'])

		norm['min'] = normalize.min(training['features'])
		norm['range'] = normalize.range(training['features'])

		training['features'] = normalize.scale(training['features'],norm['min'],norm['range'])
		testing['features'] = normalize.scale(testing['features'],norm['min'],norm['range'])

		f = open("%s%s.normalize.json" % (config.output,config.name), 'w')
		normalize.encode(norm,f)
		f.close()

	config.insize = insize

	return (training,testing,config)

if __name__ == "__main__":
	elapsed = []
	for i in range(0,5):
		(training,testing,config) = parseInput()

		# Create a Training and Validation set
		num_samples = training['features'].shape[0]
		validation = {}
		[training['features'],_,validation['features']] = np.split(training['features'],[int(num_samples*0.8),int(num_samples*0.8)])
		[training['labels'],_,validation['labels']] = np.split(training['labels'],[int(num_samples*0.8),int(num_samples*0.8)])

		training_data = training['features'].T
		training_labels = training['labels'].T
		validation_data = validation['features'].T
		validation_labels = validation['labels'].T

		testing_data = testing['features'].T
		testing_labels = testing['labels'].T

		start = time.perf_counter()
		(best_pop,rankings,sserrors) = main(training_data,validation_data,config)
		diff = time.perf_counter() - start
		print("Run %02d:" % (i),diff)
		elapsed.append(diff)
		rankClassifyPlot(training,testing,best_pop,rankings,sserrors,config)
	elapsed = np.array(elapsed)
	print("Avg Runtime:",np.mean(elapsed))
	print("Min Runtime:",elapsed.min())
	print("Max Runtime:",elapsed.max())
	print("Std Runtime:",np.std(elapsed))
