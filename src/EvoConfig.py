'''

EvoConfig - Configuration for EvoAE

@author Sean Lander
@copyright 2014
@title EvoConfig

Each Auto Encoder has the ability to take a sample
and rebuild it. Auto Encoders are a collection
of Features and biases.

'''

import json

class EvoConfig:

	def __init__(self, alpha=0.01, momentum=3, lamb=3e-3,
		batchsize=None, autobatch=False, minibatch=False, finaltrain='None',
		populationsize=50, generations=100, epochs=100,
		insize=None, hsize=200, hstddev=None, hplusminus=None,
		mutation=0.1, output=None,name=None,config=None):

		self.alpha = alpha
		self.momentum = momentum
		self.lamb = lamb
		self.batchsize = batchsize
		self.autobatch = autobatch
		self.minibatch = minibatch
		self.finaltrain = finaltrain
		self.populationsize = populationsize
		self.generations = generations
		self.epochs = epochs
		self.insize = insize
		self.hsize = hsize
		self.hstddev = hstddev
		self.hplusminus = hplusminus
		self.mutation = mutation
		self.output = output
		self.name = name
		
		if config:
			conf = json.load(config)
			if 'alpha' in conf:
				self.alpha = conf['alpha']
			if 'momentum' in conf:
				self.momentum = conf['momentum']
			if 'lamb' in conf:
				self.lamb = conf['lamb']
			if 'batchsize' in conf:
				self.batchsize = conf['batchsize']
			if 'autobatch' in conf:
				self.autobatch = conf['autobatch']
			if 'minibatch' in conf:
				self.minibatch = conf['minibatch']
			if 'finaltrain' in conf:
				self.finaltrain = conf['finaltrain']
			if 'populationsize' in conf:
				self.populationsize = conf['populationsize']
			if 'generations' in conf:
				self.generations = conf['generations']
			if 'epochs' in conf:
				self.epochs = conf['epochs']
			if 'hsize' in conf:
				self.hsize = conf['hsize']
			if 'hstddev' in conf:
				self.hstddev = conf['hstddev']
			if 'hplusminus' in conf:
				self.hplusminus = conf['hplusminus']
			if 'mutation' in conf:
				self.mutation = conf['mutation']
			if 'output' in conf:
				self.output = conf['output']
			if 'name' in conf:
				self.name = conf['name']

	def encode(config,fp):
		tmp = {}
		tmp['alpha'] = config.alpha
		tmp['momentum'] = config.momentum
		tmp['lamb'] = config.lamb
		tmp['batchsize'] = config.batchsize
		tmp['autobatch'] = config.autobatch
		tmp['minibatch'] = config.minibatch
		tmp['finaltrain'] = config.finaltrain
		tmp['populationsize'] = config.populationsize
		tmp['generations'] = config.generations
		tmp['epochs'] = config.epochs
		tmp['hsize'] = config.hsize
		if config.hstddev:
			tmp['hstddev'] = config.hstddev
		if config.hplusminus:
			tmp['hplusminus'] = config.hplusminus
		tmp['mutation'] = config.mutation
		tmp['name'] = config.name
		json.dump(tmp,fp)