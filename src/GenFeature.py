'''

GenFeature - Genetic Algorithm Feature for EvoAE Auto Encoder

@author Sean Lander
@copyright 2014
@title EvoFeature

This is a feature object which belongs to a single
Auto Encoder. Each feature contains all weights from
the inputs to the feature nodes, and then from the
feature nodes to the reconstruction.

'''

from numpy import *

class EvoFeature:

	'''
	# Class Variables
	insize		-> int 		: number of inputs
	hiddensize 	-> int 		: number of hidden nodes
	Win 		-> array()	: array of incoming weights
	bin			-> array()	: array of incoming biases
	Wout		-> array()	: array of outgoing weights
	bout		-> array()	: array of outgoing biases
	'''

	def __init__(self,insize=None,hiddensize=None,Win=None,bin=None,Wout=None,bout=None):
		self.insize = insize
		if not self.insize:
			raise BaseException("No insize provided to GenFeature")

		self.hiddensize = hiddensize
		if not self.hiddensize:
			raise BaseException("No hiddensize provided to GenFeature")

		self.Win = Win
		self.bin = bin
		self.Wout = Wout
		self.bout = bout
		if not self.Win:
			self.Win = random.normal(0,1,(self.hiddensize,self.insize))
		if not self.bin:
			self.bin = random.normal(0,1,(self.hiddensize,1))
		if not self.Wout:
			self.Wout = random.normal(0,1,(self.insize,self.hiddensize))
		if not self.bout:
			self.bout = random.normal(0,1,(self.insize,1))
		print("EvoFeature Created")