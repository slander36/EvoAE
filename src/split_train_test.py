'''

Split Train Test - Splits delimited file into separate training and testing files

@author Sean Lander
@copyright 2014
@title split_train_test

Pass a csv, its delimiter, and if you want to shuffle it before splitting
it into a training file and testing file

'''

import numpy as np

import argparse

def main(datafile,delimiter,shuffle=False):
	data = np.loadtxt(datafile,delimiter=delimiter)
	if shuffle:
		np.random.shuffle(data)
	n_samples = data.shape[0]
	training = data[0:0.7*n_samples]
	testing = data[0.7*n_samples:]
	fname = datafile.name.split('.')
	f_training = open('.'.join(fname[:-1])+'_training.'+fname[-1],'wb')
	f_testing = open('.'.join(fname[:-1])+'_testing.'+fname[-1],'wb')
	np.savetxt(f_training,training,delimiter=',',fmt='%f')
	np.savetxt(f_testing,testing,delimiter=',',fmt='%f')
	f_training.close()
	f_testing.close()


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("datafile",type=argparse.FileType(),help="data to be parsed")
	parser.add_argument("delimiter",help="delimiter",choices=[',',' ','|','\t'])
	parser.add_argument("-s","--shuffle",help="shuffle data",action="store_true")

	args = parser.parse_args()

	main(args.datafile,args.delimiter,args.shuffle)