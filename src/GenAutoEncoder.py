'''

GenAutoEncoder - Genetic Algorithm Auto Encoder for EvoAE

@author Sean Lander
@copyright 2014
@title GenAutoEncoder

Each Auto Encoder has the ability to take a sample
and rebuild it. Auto Encoders are a collection
of Features and biases.

'''

from numpy import *

from AutoEncoder import AutoEncoder
from GenFeature import GenFeature

class GenAutoEncoder(AutoEncoder):

	'''
	# Class Variables
	features	-> Obj		: GenFeature
	config		-> Obj		: EvoConfig
	'''

	def __init__(self, features=None, config=None):

		super(EvoAutoEncoder,self).__init__(config)

		self.features = features

		if not self.features:
			self.features = GenFeature(insize=self.config.insize,hiddensize=round(self.config.insize*0.2))

		print("EvoAutoEncoder Created")