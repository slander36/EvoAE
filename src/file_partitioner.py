'''
Break a file into quarters
'''

import argparse

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('partitions',type=int)
	parser.add_argument('input',type=argparse.FileType('r'))
	parser.add_argument('output',type=str)
	args = parser.parse_args()
	data = []
	for line in args.input:
		data.append(line)

	files = []

	for i in range(args.partitions):
		files.append(open('%d-%s' % (i,args.output),'w'))

	counter = 0
	for line in data:
		files[counter].write('%s\n' % (line))
		counter = (counter + 1) % args.partitions
	
	for i in range(args.partitions):
		files[i].close()

if __name__ == '__main__':
	main()