'''

EvoAutoEncoder - Evolutionary Algorithm Auto Encoder for EvoAE

@author Sean Lander
@copyright 2014
@title EvoAutoEncoder

Each Auto Encoder has the ability to take a sample
and rebuild it. Auto Encoders are a collection
of Features and biases.

'''

from numpy import *

from utils import *
import copy
import math

import json

from AutoEncoder import AutoEncoder
from EvoFeature import EvoFeature

class EvoAutoEncoder(AutoEncoder):

	DEBUG = False

	'''
	# Class Variables
	features	-> []		: vector of new features
	bout 		-> array()	: vector of hidden layer biases
	config		-> Obj		: EvoConfig
	'''

	def __init__(self, features=None, bout=None, config=None, gen=0, id=0, debug=False):
		global DEBUG
		DEBUG = debug

		super(EvoAutoEncoder,self).__init__(config=config, id=id, debug=debug)

		self.gen = gen

		self.features = features
		self.bout = bout

		if not self.features:
			f = []
			# Select hidden size. Limit minimum size to 1
			if self.config.hstddev is not None:
				hsize = round(random.normal(self.config.hsize,self.config.hstddev))
			elif self.config.hplusminus is not None:
				hsize = round(random.uniform(self.config.hsize-self.config.hplusminus,self.config.hsize+self.config.hplusminus))
			else:
				hsize = self.config.hsize
			hsize = hsize if hsize > 2 else 2
			self.size = round(hsize)

			# Build the hidden layer
			for i in range(0,self.size):
				f.append(EvoFeature(insize=self.config.insize))
			self.features = array(f)
		else:
			self.size = len(self.features)

		if not self.bout:
			self.bout = random.normal(0,0.5,(self.config.insize,1))

		# print("EvoAutoEncoder Created")

	def getWeights(self):
		self.weightsFromFeatures()
		bout = self.bout
		return True

	def weightsFromFeatures(self):
		# print("Weights from Features")
		wins = []
		bins = []
		wouts = []
		for feature in self.features:
			wins.append(feature.Win)
			bins.append(feature.bin)
			wouts.append(feature.Wout)
		self.Win = row_stack(wins)
		self.bin = row_stack(bins)
		self.Wout = column_stack(wouts)


	def featuresFromWeights(self):
		# print("Feautres from Weights")
		if self.Win.shape[0] != self.Wout.shape[1] or \
		   self.Win.shape[0] != self.size or \
		   self.Wout.shape[1] != self.size:
			raise BaseException("Cannot get features from weights. Dimension mis-match")

		Win = self.Win
		bin = self.bin
		Wout = transpose(self.Wout)

		for i in range(self.size):
			self.features[i].Win = Win[i]
			self.features[i].bin = bin[i]
			self.features[i].Wout = Wout[i]
		

	def backprop(self,x,alpha,zh,ah,zx,ax,error,o_Dwin=0,o_Dbin=0,o_Dwout=0,o_Dbout=0):
		# print("Back propogation")
		(Dwin,Dbin,Dwout,Dbout) = super(EvoAutoEncoder,self).backprop(x,alpha,zh,ah,zx,ax,error,o_Dwin,o_Dbin,o_Dwout,o_Dbout)

		# print("Get the features from the new weights")
		self.featuresFromWeights()

		return (Dwin,Dbin,Dwout,Dbout)

	def search(self,training_data):
		global DEBUG

		epochs_range = range(0,self.config.epochs)
		if DEBUG:
			print('GEN,AE,ITER - Previo Error|Curren Error|Difference')
		# For each AE pop[i], run J epochs
		(Dwin,Dbin,Dwout,Dbout) = (0,0,0,0)
		last_error = 1e1000
		sserrors = []
		for j in epochs_range:
			# Feed forward to get outputs of hidden and output layer
			(zin,ain,zout,aout) = self.feedforward(training_data)

			# Generate the error
			error = (training_data - aout)
			sserror = np.average(np.sum(error**2,axis=0))
			if DEBUG:
				print("%03d,%02d,%04d - %03.8f|%03.8f|%3.8f" % (self.gen,self.id,j,last_error,sserror,last_error-sserror))
			last_error = sserror
			sserrors.append(sserror)

			# Backprop
			#alpha = self.config.alpha # Base version
			alpha = self.config.alpha*(1+math.exp(-self.gen)) # Version 1
			#alpha = self.config.alpha*(1+math.exp(-j)) # Version 2
			(Dwin,Dbin,Dwout,Dbout) = self.backprop(training_data,alpha,zin,ain,zout,aout,error,Dwin,Dbin,Dwout,Dbout)
		return (sserrors,last_error)

	def mutate(self,mutation):
		# See if we will mutate
		if random.random() < self.config.mutation:
			# Time to mutate
			if random.random() > 0.5:
				# Add a node
				self.features.append(mutation)
			else:
				# Remove a node
				loser = random.randint(self.size)
				self.features.pop(loser)
			self.size = len(self.features)

	def randomFeature(self):
		i = np.random.randint(len(self.features))
		return self.features[i]

	def crossover(p1,p2):
		p1size = len(p1.features)
		p2size = len(p2.features)

		if p1size > p2size:
			max_len = p1size
			min_len = p2size
		else:
			max_len = p2size
			min_len = p1size

		mask = random.rand(max_len,)
		mask = mask > 0.5

		c1 = []
		c2 = []

		for i in range(max_len):
			if i < p1size:
				if mask[i]:
					c1.append(p1.features[i])
				else:
					c2.append(p1.features[i])
			if i < p2size:
				if mask[i]:
					c2.append(p2.features[i])
				else:
					c1.append(p2.features[i])
		return (c1,c2)

	def encode(ae,fp):
		tmp = {}
		tmp['features'] = []
		for f in ae.features:
			Win = f.Win.tolist()
			bin = f.bin.tolist()
			Wout = f.Wout.tolist()
			feature = {'Win':Win,'bin':bin,'Wout':Wout}
			tmp['features'].append(feature)

		tmp['bout'] = ae.bout.tolist()

		json.dump(tmp,fp)

	def decode(fp):
		tmp = json.load(fp)
		features = []
		for f in tmp['features']:
			Win = array(f['Win'])
			bin = array(f['bin'])
			Wout = array(f['Wout'])
			features.append(EvoFeature(Win=Win,bin=bin,Wout=Wout))
		bout = array(tmp['bout'])
		return EvoAutoEncoder(features=features,bout=bout)