'''

EvoFeature - Evolutionary Algorithm Feature for EvoAE Auto Encoder

@author Sean Lander
@copyright 2014
@title EvoFeature

This is a feature object which belongs to a single
Auto Encoder. Each feature contains the weights from
the inputs to the feature node, and then from the
feature node to the reconstruction.

'''

from numpy import *

class EvoFeature:

	'''
	# Class Variables
	insize		-> int 		: number of inputs
	Win 		-> array()	: array of incoming weights
	bin			-> array()	: incoming bias
	Wout		-> array()	: array of outgoing weights
	'''

	def __init__(self,Win=None,bin=None,Wout=None,insize=None):
		self.Win = Win
		self.bin = bin
		self.Wout = Wout
		
		self.insize = insize

		if not self.Win:
			if not self.insize:
				raise BaseException("No insize provided to EvoFeature")
			self.Win = random.normal(0,0.5,(1,self.insize))
		if not self.bin:
			if not self.insize:
				raise BaseException("No insize provided to EvoFeature")
			self.bin = random.normal(0,0.5,(1,1))
		if not self.Wout:
			if not self.insize:
				raise BaseException("No insize provided to EvoFeature")
			self.Wout = random.normal(0,0.5,(self.insize,1))
		# print("EvoFeature Created")