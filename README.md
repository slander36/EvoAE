EvoEA
=====

Evolutionary Auto Encoder for Feature Creation and Reduction

Required Modeules
=================
All modules can be found at the [Unofficial Windows Binaries for Python Extension Packages](http://www.lfd.uci.edu/~gohlke/pythonlibs/) site  
[Numpy](http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy)  
[Scipy](http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy)  
[Scikit-learn](http://www.lfd.uci.edu/~gohlke/pythonlibs/#scikit-learn) 
[MatPlotLib](http://www.lfd.uci.edu/~gohlke/pythonlibs/#matplotlib) 
  
File Required for Numby, Scipy and Scikit-learn:  
[Pyparsing](http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyparsing)  
[Python-Dateutil](http://www.lfd.uci.edu/~gohlke/pythonlibs/#python-dateutil)  
[pytz](http://www.lfd.uci.edu/~gohlke/pythonlibs/#pytz)  
[Setuptools](http://www.lfd.uci.edu/~gohlke/pythonlibs/#setuptools)  
[six](http://www.lfd.uci.edu/~gohlke/pythonlibs/#six)  

To Run
======

##### Make sure required modules are installed  

## Testing and Analyzing  
  
You can use EvoAE.py in order to run analysis on the Autoencoder population you are creating. You can do this by either running each by hand, using the instructions below, or by creating batch/bash files to run multiple in serial.  
  
Using a config file is the preferred method, as it is the simplest to update, otherwise there are numerous options that can be selected by hand as well. Command line parameters are priority over config files for on-the-fly testing.

#### File Format  
Data should be formatted with the label as the first column as a number or string, with the following columns containing feature data.  

| Label | Feature 1 | Feature 2 | ... |
|-------|-----------|-----------|-----|
| 'A'   | 0.3324    | 2331.3    | ... |

```bash
$ python EvoAE.py -h
usage: EvoAE.py [-h] [--mnist] [--mnisttrain MNISTTRAIN]
                [--mnisttest MNISTTEST] [-a ALPHA] [-m MOMENTUM] [-l LAMB]
                [-s HSIZE] [-d HSTDDEV] [-+ HPLUSMINUS] [-u MUTATION]
                [-p POPULATIONSIZE] [-g GENERATIONS] [-e EPOCHS]
                [--batchsize BATCHSIZE] [--autobatch] [--minibatch]
                [--finaltrain {All,Batch,None}] [-n] [-o OUTPUT] [-t NAME]
                [-v] [-c CONFIG]
                training testing

Runner for Evolutionary AutoEncoder training and testing

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        configuration file

Data:
  training              data for training
  testing               data for testing

Configuration:
  --mnist               use the MNIST data set instead
  --mnisttrain MNISTTRAIN
                        number of training samples for mnist
  --mnisttest MNISTTEST
                        number of testing samples for mnist
  -a ALPHA, --alpha ALPHA
                        learning rate
  -m MOMENTUM, --momentum MOMENTUM
                        momentum term for congugate gradient descence (0 to
                        turn off)
  -l LAMB, --lamb LAMB  lambda term for weight decay (0 to turn off)
  -s HSIZE, --hsize HSIZE
                        size of the hidden layer
  -d HSTDDEV, --hstddev HSTDDEV
                        stddev of hidden layer size on initialization
  -+ HPLUSMINUS, --hplusminus HPLUSMINUS
                        +/- of hidden layer size on initialization
  -u MUTATION, --mutation MUTATION
                        mutation rate [0,1] scale
  -p POPULATIONSIZE, --populationsize POPULATIONSIZE
                        number of organisms in the population
  -g GENERATIONS, --generations GENERATIONS
                        number of generations
  -e EPOCHS, --epochs EPOCHS
                        numer of search iterations
  --batchsize BATCHSIZE
                        the size of each batch used in training (optional)
  --autobatch           splits data into populationsize many batches
                        (optional)
  --minibatch           splits data into populationsize many batches, with
                        each member training against a single batch (optional)
  --finaltrain {All,Batch,None}
                        specify if you want a final round of training after
                        convergence and which type (optional)
  -n, --normalize       flag for if data needs to be normalized before
                        training
  -o OUTPUT, --output OUTPUT
                        output directory name
  -t NAME, --name NAME  name of experiment
  -v, --verbose         verbose output

# Example

$ python EvoAE.py -n -c minibatch-none-heartdisease.config ..\data\heartdisease\processed.cleveland_training.csv ..\data\heartdisease\processed.cleveland_testing.csv >> Small-Minibatch-None.txt
```

## Feature Reduction  
  
If you trust your data to be reduced, you can go straight to feature reduction using this tool. This will take in unlabeled data and create a population of Autoencoders, using the Autoencoder with the lowest reconstruction error in order to generate a reduced data set.  

```bash
$ python FeatureReducer.py -h
usage: FeatureReducer.py [-h] [-a ALPHA] [-m MOMENTUM] [-l LAMB] [-s HSIZE]
                         [-d HSTDDEV] [-+ HPLUSMINUS] [-u MUTATION]
                         [-p POPULATIONSIZE] [-g GENERATIONS] [-e EPOCHS]
                         [--batchsize BATCHSIZE] [--autobatch] [--minibatch]
                         [--finaltrain {All,Batch,None}] [-n] [-o OUTPUT]
                         [-t NAME] [-v] [-c CONFIG]
                         training

Runner for Evolutionary AutoEncoder training and testing

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        configuration file

Data:
  training              data for training

Configuration:
  -a ALPHA, --alpha ALPHA
                        learning rate
  -m MOMENTUM, --momentum MOMENTUM
                        momentum term for congugate gradient descence (0 to
                        turn off)
  -l LAMB, --lamb LAMB  lambda term for weight decay (0 to turn off)
  -s HSIZE, --hsize HSIZE
                        size of the hidden layer
  -d HSTDDEV, --hstddev HSTDDEV
                        stddev of hidden layer size on initialization
  -+ HPLUSMINUS, --hplusminus HPLUSMINUS
                        +/- of hidden layer size on initialization
  -u MUTATION, --mutation MUTATION
                        mutation rate [0,1] scale
  -p POPULATIONSIZE, --populationsize POPULATIONSIZE
                        number of organisms in the population
  -g GENERATIONS, --generations GENERATIONS
                        number of generations
  -e EPOCHS, --epochs EPOCHS
                        numer of search iterations
  --batchsize BATCHSIZE
                        the size of each batch used in training (optional)
  --autobatch           splits data into populationsize many batches
                        (optional)
  --minibatch           splits data into populationsize many batches, with
                        each member training against a single batch (optional)
  --finaltrain {All,Batch,None}
                        specify if you want a final round of training after
                        convergence and which type (optional)
  -n, --normalize       flag for if data needs to be normalized before
                        training
  -o OUTPUT, --output OUTPUT
                        output directory name
  -t NAME, --name NAME  name of experiment
  -v, --verbose         verbose output

# Example

$ python FeatureReducer.py -n -c Terms2.config -o "../output/Terms438-200-2" "..\output\Terms200\Reduced.csv" --verbose
```

#### File Format  
Data should be formatted with the label as the first column as a number or string, with the following columns containing feature data.  

| Feature 1 | Feature 2 | ... |
|-----------|-----------|-----|
| 0.3324    | 2331.3    | ... |

Configuration File
==================

## Example config file:  

```json
{
  "alpha":0.1,
  "momentum":2,
  "populationsize":50,
  "generations":50,
  "epochs":20,
  "hsize":200,
  "hstddev":50,
  "mutation":0.1,
  "output":"../output/Terms200",
  "minibatch":true,
  "finaltrain":"None",
  "name":"Terms200"
}
```